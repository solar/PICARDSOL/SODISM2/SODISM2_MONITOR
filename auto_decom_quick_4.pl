#!/usr/bin/perl
use strict;
use File::Copy;
use Proc::Background;
use Win32;
BEGIN {
                Win32::SetChildShowWindow(0)
                        if defined &Win32::SetChildShowWindow
        };
if($#ARGV < 0 || $#ARGV >1){ die "auto_decom_quick_3.pl: Wrong number of arguments. \n". 
                                  "Usage: auto_decom.pl plan_name [stdout-stderr_filename] \n"}		
my $plan=$ARGV[0];	
#my $iscal=($plan=~m/CAL/);	# Stellar plan name must include CAL in their name
$plan=~m/^(.*)\[(.*)\]/;
$plan=$1;
my $memo=$2;
if($#ARGV ==1)
{
	my $fichier_STDOUT_STDERR=$ARGV[1];
	open STDOUT, '>>', $fichier_STDOUT_STDERR or die "Cannot redirect STDOUT to $fichier_STDOUT_STDERR $!";
	open STDERR, '>>', $fichier_STDOUT_STDERR or die "Cannot redirect STDERR to $fichier_STDOUT_STDERR $!";;
	my $old_fd = select(STDOUT);
	$|=1;
	select($old_fd);
}	

my $root_dir='D:\\Users\\Sodism2.BV-PICARD-HOST2\\AcquisitionSodism2\\';
my $dir_JourneeType="${root_dir}JourneeType\\";
my $decom_dir="C:\\cygwin\\home\\Sodism2\\QUICKLOOK_TC\\QUICKLOOK\\exec\\";#"${root_dir}DecomType\\";
my $full_decom_dir="C:\\cygwin\\home\\Sodism2\\QUICKLOOK\\exec\\";
my $decom_exe="${decom_dir}Quick_LOOK.exe";#"${decom_dir}DecomTMimage.exe";
my $full_decom_exe="${full_decom_dir}Quick_LOOK.exe";
my $dir_VMSHARE='C:\\VMSHARE\\';

my $co_st="*_CO_*.fits";#"*.llc.rec.fit";
#my $im_st="*_RS_*.fits";#"*.cmp.rec.fit";
my $allfit_st="*.fits";#"*.rec.fit";


chdir $dir_JourneeType or die "Cannot go to directory $dir_JourneeType $!";
print "$plan : WAITING FOR BIN file in JourneeType....\n";
my @files;
#Wait for .bin files in JourneeType
do{
    @files=glob("*.bin");
    sleep(5);
}until($#files!=-1);

#Take the last bin file
@files=sort @files;
my $f=$files[-1];
print "$plan : found: $f\n";

#Extract Date and Time from last .bin file
my $time=substr($f,-8,4);
my $date=substr($f,-17,8);

#Create Plan subdirectories
if(!-e "$root_dir$date"){mkdir "$root_dir$date" or die "Cannot create directory $root_dir$date $!"}

my $dir_plan=($memo eq '')?"$root_dir$date\\${date}_${time}\\":"$root_dir$date\\${date}_${time}_${memo}\\";
if(!-e $dir_plan){mkdir $dir_plan or die "Cannot create directory $dir_plan $!"}

my $dir_brut="${dir_plan}Brut\\";
if(!-e $dir_brut){mkdir $dir_brut or die "Cannot create directory $dir_brut $!"}

my $dir_decom="${dir_plan}Decom\\";
if(!-e $dir_decom){mkdir $dir_decom or die "Cannot create directory $dir_decom $!"}

#copy bin file in Plan subdirectory and VMSHARE (so that centraliseur knows date of start and plan name)
copy ("$dir_JourneeType$f", "$dir_brut$f") or die "Cannot copy $dir_JourneeType$f into $dir_brut$f $!";
copy ("$dir_JourneeType$f", "$dir_VMSHARE$f") or die "Cannot copy $dir_JourneeType$f into $dir_VMSHARE$f $!";

print "$plan : Start AutoDecom....\n";
my $fz_sav=0;
my $copy_done=0;
my $decom_ok=0;
my $fz_bad=0;
while( &proc_exist("userver.exe") and -e "$dir_JourneeType$f")  #same bin file must still be in JourneeType dir
{	                                                          #otherwise its a new plan that is running
    #Copy all .bru files from JourneeType to Brut subdirectory
    chdir $dir_JourneeType or die "Cannot go to directory $dir_JourneeType $!";
    foreach (glob("*.bru")){copy ("$_", "$dir_brut$_") or warn "Cannot copy $_ into $dir_brut$_ $!"}
    
    #Look for P*S.bru files in Brut subdirectory , sort and take the last one
    chdir $dir_brut or die "Cannot go to directory $dir_brut $!";
    my @Sbru_files=sort(glob("${plan}*S.bru"));
	#print "here $#Sbru_files $dir_brut ${plan}*S.bru\n";
    if ($#Sbru_files!=-1){
		my $last_bru=$Sbru_files[-1];
		my $fz= (-s "$dir_brut$last_bru")/1024;
		#print "$fz\n";
		#if($fz > 1024 && $fz==$fz_sav && $copy_done!=$fz && $fz!=$fz_bad){
		if($fz > 600 && $copy_done!=$fz && $fz!=$fz_sav){
	    
			#do Decom in Decom subdirectory
			chdir $dir_decom or die "Cannot go to directory $dir_decom $!";
			#print "$decom_exe -in $dir_brut$last_bru \n";
			#print "OK\n";
			$decom_ok=1;
			my $log_decom="decom.log";
			&clean_decom();
			!system("$decom_exe -in $dir_brut$last_bru -rep . > $log_decom  2>&1") or $decom_ok=1;  #We don't want to die if it is interupted
			if($decom_ok){
				#print "DECOM OK\n";
				open DECOMLOG,$log_decom;
				while(<DECOMLOG>) {
					if(/ATTENTION PQ MANQUANT/){
						$decom_ok=0;
						$fz_bad=$fz;
						#print "ATTENTION PQ MANQUANT\n";
						last;
					}
				}
				close DECOMLOG;
			}
			#if($decom_ok){
		    if(1){
				#Clean Decom subdirectory	   
				#&clean_decom();
		
				#Look for .cmp.rec.fit files in Decom subdirectory , 
				#sort and copy the last one to VMSHARE directory
				#if(!$iscal){
					my @co_files=sort {($a=~m/(\d{8}_\d{4})/)[0] cmp ($b=~m/(\d{8}_\d{4})/)[0]} glob($co_st);
					if($#co_files != -1){
						if($decom_ok || ($#co_files > 0)){
							my $last_co=$co_files[($decom_ok)?-1:-2];
							if(!-e "$dir_VMSHARE$last_co"){
								print "$plan : Copy $last_co to $dir_VMSHARE\n";
								copy("$last_co","$dir_VMSHARE$last_co") or die "Cannot copy $last_co into $dir_VMSHARE$last_co $!";
								#!system("convert -rotate 90 -flip $last_co  $dir_VMSHARE$last_co") or die "Cannot copy $last_co into $dir_VMSHARE$last_co $!";
								$copy_done=$fz;
							}
						}		
					}
				#}	
				#my @fit_files=($iscal)?sort {($a=~m/(\d{8}_\d{4})/)[0] cmp ($b=~m/(\d{8}_\d{4})/)[0]} glob($allfit_st):sort {($a=~m/(\d{8}_\d{4})/)[0] cmp ($b=~m/(\d{8}_\d{4})/)[0]} glob($im_st) ;  #allow .llc.rec.fit for stellar plan
				my @fit_files=sort {($a=~m/(\d{8}_\d{4})/)[0] cmp ($b=~m/(\d{8}_\d{4})/)[0]} glob($allfit_st);
				if($#fit_files != -1){
					if($decom_ok || ($#fit_files > 0)){
						my $last_fit=$fit_files[($decom_ok)?-1:-2];
						#print "here: $last_fit\n";
						if(!-e "$dir_VMSHARE$last_fit"){
							print "$plan : Copy $last_fit to $dir_VMSHARE\n";
                                                        copy("$last_fit", "$dir_VMSHARE$last_fit") or die "Cannot copy $last_fit into $dir_VMSHARE$last_fit $!";
							#!system("convert -rotate 90 -flip $last_fit  $dir_VMSHARE$last_fit") or die "Cannot copy $last_fit into $dir_VMSHARE$last_fit $!";
							$copy_done=$fz;
						}
					}	
				}
			}
		}else{
			sleep(1)
		}
		$fz_sav=$fz;	
	}
}

print "$plan : --------NOW I WILL DECOMUTE ALL AND CLEAN------------\n";
#print "---------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!---------------\n";
#print "---------------!!!!!KEEP THIS WINDOW OPEN!!!!!!!!---------------\n";
#print "---------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!---------------\n";
#print "--------------YOU CAN IMMEDIATLY LAUNCH A NEW PLAN--------------\n";
#print "-------OR WAIT THE END OF DECOM IF IT WAS THE LAST PLAN---------\n";
#print "----------------------------------------------------------------\n\n\n";

#Decomute all .bru files using the first one
sleep(5);
&decom_final();

print "---------------------------------------------------------------\n";
print "$plan :-----------------------ALL DECOMUTED ---------------\n";
print "---------------------------------------------------------------\n";


sub proc_exist
{
#Test if a process given by its name is running on WIN32 system
# eg:  if(&proc_exist("firefox.exe")) {print "firefox is running\n"};
# Thierry Corbard, corbard@oca.eu 2011
    use Win32::Process::List;
    
    my $P = Win32::Process::List->new();  #constructor
    my %list = $P->GetProcesses();        #returns the hashes with PID and process name
    my $proc_name=$_[0];
    my @PID = grep { $list{$_} eq $proc_name } keys %list;
    return ($#PID  >= 0) ;
}

sub clean_decom
{
    #Look for .cmp and .cmp.rec files in decom subdirectory and remove them
    #print "Clean Decom subdirectory \n";
    unlink (glob("${dir_decom}*"));	
}


sub decom_final
{    
    #Look for .out files in Brut subdirectory and remove them
    chdir $dir_brut or die "Cannot go to directory $dir_brut $!";
    unlink(glob("*.out"));
	#Look for P*S.bru files in Brut subdirectory , 
    #sort and take the first one	
    
	my $catfile='cat.bru';
	my $log_decom='decom.log';
	#print "copy /b ${dir_brut}${plan}*S.bru $dir_brut$catfile \n";
	#!system("copy /b ${dir_brut}${plan}*S.bru $dir_brut$catfile") or die "Cannot cat the bru files $!";
	#my $cat_proc=Proc::Background->new("copy /b ${dir_brut}${plan}*S.bru $dir_brut$catfile");
	#$cat_proc->wait;
	$/= \4096;
	open(OUT,"> $dir_brut$catfile") or die("can't create output file : $!");
	binmode(OUT);
	while(glob("${dir_brut}${plan}*S.bru")){
		open(IN,"< $_") or die("can't create input file $_ : $!");
		binmode(IN);
		{
			print OUT $_ while <IN>;
		}
		close IN;
	}	
	close OUT;
	#my $first_bru=$Sbru_files[0];
	#do Decom in Decom subdirectory
	chdir $dir_decom or die "Cannot go to directory $dir_decom $!";
	print "$plan : Decomute $catfile ....\n";
	#Clean Decom directory
    &clean_decom();
	!system ("$full_decom_exe -in $dir_brut$catfile -rep . > $log_decom  2>&1") or warn "exit: $? $!";
	print "$plan : Decomute ALL done\n";
	
    #}else{
	#print "$plan : done : no P*S.bru file found to decomute\n";
    #}
	

    #Look for .out files in Brut subdirectory and move them to Decom	
    #print "Move .out from Brut to Decom subdirectory \n";    
    chdir $dir_brut or die "Cannot go to directory $dir_brut $!";
    #foreach (glob("*.out")){move("$_","$dir_decom$_") or die "Cannot move $_ to $dir_decom$_ $!"};

	#Remove the cat file
	unlink($catfile);
	
    #Look for session*.bru in Brut subdirectory and remove them
    unlink(glob("session*.bru")) ;
	
    #Look for .llc and .llc.rec files  in decom subdirectory and delete
    #chdir $dir_decom or die "Cannot go to directory $dir_decom $!";
    #unlink(glob("*.llc"));
    #unlink(glob("*.llc.rec")) ;
}
